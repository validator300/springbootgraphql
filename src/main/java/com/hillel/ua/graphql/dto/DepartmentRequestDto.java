package com.hillel.ua.graphql.dto;

public record DepartmentRequestDto(String name, Integer organizationId){}