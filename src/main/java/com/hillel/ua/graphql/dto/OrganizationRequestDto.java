package com.hillel.ua.graphql.dto;

public record OrganizationRequestDto(String name){}
