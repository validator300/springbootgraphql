package com.hillel.ua.graphql.dto;

public record EmployeeRequestDto(String firstName, String lastName, String position, int salary, int age,
                                 Integer departmentId, Integer organizationId){}