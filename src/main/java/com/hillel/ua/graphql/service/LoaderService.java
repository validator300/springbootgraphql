package com.hillel.ua.graphql.service;

public interface LoaderService {
    void generateData();

    long count();

}
