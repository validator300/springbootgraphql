package com.hillel.ua.graphql.service;

import com.github.javafaker.Faker;
import com.hillel.ua.graphql.controller.EmployeeController;
import com.hillel.ua.graphql.dto.EmployeeRequestDto;
import com.hillel.ua.graphql.repository.EmployeeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;

@Slf4j
@Service
public class LoaderServiceBean implements LoaderService {

    private EmployeeController controller;
    private EmployeeRepository employeeRepository;

    @Override
    public void generateData() {
        List<EmployeeRequestDto> employees = createListEmployees();
        controller.newEmployees(employees);
    }

    @Override
    public long count() {
        return employeeRepository.count();
    }

    public List<EmployeeRequestDto> createListEmployees() {
        List<EmployeeRequestDto> employees = new ArrayList<>();
        long seed = 1;
        Faker faker = new Faker(new Locale("en"), new Random(seed));
        for (int i = 0; i < 200_000; i++) {
            String firstName = faker.name().name();
            String lastName = faker.name().name();
            String position = "true";
            int salary = faker.number().numberBetween(0, 10000);
            int age = faker.number().numberBetween(18, 50);
            EmployeeRequestDto employee = new EmployeeRequestDto(firstName, lastName, position,
                    salary, age, 1, 1);
            controller.newEmployee(employee);
        }
        return employees;
    }
}
