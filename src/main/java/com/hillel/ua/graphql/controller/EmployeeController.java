package com.hillel.ua.graphql.controller;

import com.hillel.ua.graphql.dto.EmployeeRequestDto;
import com.hillel.ua.graphql.entities.Department;
import com.hillel.ua.graphql.entities.Employee;
import com.hillel.ua.graphql.entities.Organization;
import com.hillel.ua.graphql.filter.EmployeeFilter;
import com.hillel.ua.graphql.filter.FilterField;
import com.hillel.ua.graphql.repository.DepartmentRepository;
import com.hillel.ua.graphql.repository.EmployeeRepository;
import com.hillel.ua.graphql.repository.OrganizationRepository;
import com.hillel.ua.graphql.service.LoaderServiceBean;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.List;

@Controller
public class EmployeeController {

    DepartmentRepository departmentRepository;
    EmployeeRepository employeeRepository;
    OrganizationRepository organizationRepository;

    EmployeeController(DepartmentRepository departmentRepository, EmployeeRepository employeeRepository, OrganizationRepository organizationRepository) {
        this.departmentRepository = departmentRepository;
        this.employeeRepository = employeeRepository;
        this.organizationRepository = organizationRepository;
    }

    @QueryMapping
    public Iterable<Employee> employees() {
        return employeeRepository.findAll();
    }

    @QueryMapping
    public Employee employee(@Argument Integer id) {
        LoaderServiceBean b = new LoaderServiceBean();
        return employeeRepository.findById(id).orElseThrow();
    }

    @QueryMapping
    public long countEmployees() {
        return employeeRepository.count();
    }

    @MutationMapping
    public Employee newEmployee(@Argument EmployeeRequestDto employee) {
        Department department = departmentRepository.findById(employee.departmentId()).get();
        Organization organization = organizationRepository.findById(employee.organizationId()).get();
        return employeeRepository.save(new Employee(null, employee.firstName(),
                employee.lastName(), employee.position(), employee.age(),
                employee.salary(), department, organization));
    }

    @MutationMapping
    public List<Employee> newEmployees(@Argument List<EmployeeRequestDto> employees) {
        List<Employee> savedEmployees = new ArrayList<>();

        for (EmployeeRequestDto employee : employees) {
            Department department = departmentRepository.findById(employee.departmentId()).orElse(null);
            Organization organization = organizationRepository.findById(employee.organizationId()).orElse(null);

            if (department != null && organization != null) {
                Employee savedEmployee = employeeRepository.save(new Employee(
                        null,
                        employee.firstName(),
                        employee.lastName(),
                        employee.position(),
                        employee.age(),
                        employee.salary(),
                        department,
                        organization
                ));
                savedEmployees.add(savedEmployee);
            }
        }
        return savedEmployees;
    }

    @QueryMapping
    public Iterable<Employee> employeesWithFilter(@Argument EmployeeFilter filter) {
        Specification<Employee> spec = null;
        if (filter.getSalary() != null) spec = bySalary(filter.getSalary());
        if (filter.getAge() != null) spec = (spec == null ? byAge(filter.getAge()) : spec.and(byAge(filter.getAge())));
        if (filter.getPosition() != null)
            spec = (spec == null ? byPosition(filter.getPosition()) : spec.and(byPosition(filter.getPosition())));
        if (spec != null) return employeeRepository.findAll(spec);
        else return employeeRepository.findAll();
    }

    private Specification<Employee> bySalary(FilterField filterField) {
        return (root, query, builder) -> filterField.generateCriteria(builder, root.get("salary"));
    }

    private Specification<Employee> byAge(FilterField filterField) {
        return (root, query, builder) -> filterField.generateCriteria(builder, root.get("age"));
    }

    private Specification<Employee> byPosition(FilterField filterField) {
        return (root, query, builder) -> filterField.generateCriteria(builder, root.get("position"));
    }
}
