package com.hillel.ua.graphql.controller;

import com.hillel.ua.graphql.service.LoaderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.stereotype.Controller;

@Controller
@Slf4j
public class LoaderController {

    private final LoaderService loaderService;

    public LoaderController(LoaderService loaderService) {
        this.loaderService = loaderService;
    }

    @MutationMapping
    public String fillDataBase() {
        loaderService.generateData();
        String count = "Amount clients: " + loaderService.count();
        log.info("fillDataBase() LoaderController - end: count = {}", count);
        return count;
    }
}
